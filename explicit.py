
def toString (n_gram):
    word = ""
    for i in range(0, len(n_gram) - 1):
        word = word + n_gram[i] + " "
    word = word + n_gram[len(n_gram) - 1]
    return word
	
def toList(t):
    lst = []
    for i in range(0, len(t) - 1):
        lst.append(t[i])
    return lst
	
def tf_idf(re_list, docs, current, curr_size):
    
    d = dict()
    
    #para o texto, testo cada um dos REs
    for n_gram in re_list:
        
        #tranformar o tuplo numa string:
        word = toString(n_gram)
        
        #frequencia da palavra no texto:
        #freq = sum(current[i:i+len(word)]==word for i in range(len(current)))
        freq = len(re.findall(word, current))
        if freq == 0:
            continue
        
        #numero de documentos onde a palavra aparece:
        occurrences = float( checkWordApearance(word, docs) )
        if occurrences == 0:
            continue
        
        #tf-idf value for current RE
        b = (float(freq) / curr_size) 
        a = math.log(len(docs) / occurrences)
        value = a * b
        
        if len(n_gram) > 1:
            d[n_gram] = value
        elif value > 0.005:
            d[n_gram] = value
     
    return d   
	
def checkWordApearance(word, Docs):
    count = 0
    for text in Docs:
        
        #if sum(text[i:i+len(word)]==word for i in range(len(text))) > 0:
        if word in text:
            count = count + 1
    return count