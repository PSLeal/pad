# -*- coding: utf-8 -*-
import re
import time
import math as math
import random
import sys

# precisamos de até 8-gram para passar as 7-gram pelo local Maxs
SEQ_MAX = 8
FREQ = 0
GLUE = 1
PLUS_1 = 2

# Extracts the text into an array of words, sepparating the symbols from the text.
# e.g. "Hey! Come, we don't..." extracts like {Hey},{!},{Come},{,},{we},{don't}.
def parseCorpus(corpus):
    regexParse = re.compile("[0-9]+\.[0-9]+|(?:http[^\s]+)|(?:[A-Z]\.)+|[\w']+(?:\.+[\w']+)*|[!\?:;,\(\)<>\[\]«»\"\\\/]+|\.(?:\.)+|\. ", re.UNICODE)
    parsedArray = regexParse.findall(corpus)
    return parsedArray
	
	
def countFreq(corpus):
    n_grams = []
    for i in range(0,8):
        n_grams.append( dict() )
        for c in range(0, len(corpus) - i):
            key = tuple(corpus[c:c+i+1])
            if key in n_grams[i]:
                value = n_grams[i].get(key)
                value[FREQ] = value[FREQ] + float(1)
                n_grams[i].update(key = value)
            else:
                n_grams[i][key] = [float(1)]
                
    return n_grams
	

def calcGlue(n_grams):
    for i in range(1, 8):
        for key in n_grams[i]:
            if(isinstance(key, tuple)):
                glue = dice_f(key, n_grams)
                n_grams[i].get(key).append(glue)
                if(i > 1):
                    for a in range(0, 2):
                        array = n_grams[i -1].get(key[a:i+a]) 
                        if len(array) == 2:
                            array.append([glue])
                        elif len(array) > 2:
                            array[PLUS_1].append(glue)

def f_word(sequence, n_grams):
    if n_grams[len(sequence) - 1].get(sequence)[FREQ] > 0:
        return float(n_grams[len(sequence) - 1].get(sequence)[FREQ] )/len(corpus)
    else:
        return 0

def scp_f(sequence, n_grams): 
    scp = 0
    for l in range(1, len(sequence)):
        word_x = sequence[:l]
        word_y = sequence[l:]
        scp = scp + float( (f_word(word_x, n_grams) * f_word(word_y, n_grams)) )
    F = (1/ float(len(sequence) - 1)) * float(scp )
    if(F == 0):
        return 0
    else:
        return float( (f_word(sequence, n_grams) * f_word(sequence, n_grams)) ) / F

def dice_f(sequence, n_grams):
    #(2 * p(x,y) ) /p(x) + p(y)
    temp = 0
    for l in range(1, len(sequence)):
        word_x = sequence[:l]
        word_y = sequence[l:]
        temp = temp + f_word(word_x, n_grams) + f_word(word_y, n_grams)
    F = (1/ float(len(sequence) - 1)) * float(temp )
    if(F == 0):
        return 0
    else:
        return float(2 * f_word(sequence, n_grams) ) / F
    
def MI_f(sequence):
    mi = 0
    for l in range(1, len(sequence)):
        word_x = sequence[:l]
        word_y = sequence[l:]
        mi = mi + float( (f_word(word_x, n_grams) * f_word(word_y, n_grams)) )
    F = (1/ float(len(sequence) - 1)) * float(mi )
    if(F == 0):
        return 0
    else:
        return math.log(float(f_word(sequence, n_grams) ) / F)
    
def hasNoSymbols(array):
    truth = True
    for n in array:
        if not str.isalpha(n):
            truth = False
    return truth

# calcular o n-1
# calcular o n+1
def localMaxs (W, n_grams):
    minus_1_set = []

    isRE = False
    
    n_gram = len(W) - 1
    word_glue = n_grams[n_gram].get(W)[GLUE]

    
    if len(W) > 2:      
        minus_1_set.append(n_grams[n_gram - 1].get(W[0:n_gram])[GLUE])
        minus_1_set.append(n_grams[n_gram - 1].get(W[1:n_gram +1])[GLUE])
   
    x = 0
    if len(W) > 2:
        x = max(minus_1_set)
    y = max(n_grams[n_gram].get(W).pop())

    if(len(W) == 2 and word_glue > y):
        isRE = True
    if(len(W) > 2 and word_glue >= x and word_glue > y):
        isRE = True
    return isRE

def extractConcepts(n_grams):
    RE = []
    for i in range(1, 7):
        for key in n_grams[i]:
            a = []
            a.extend(key)
            if (isinstance(key, tuple) and n_grams[i].get(key)[FREQ] > 2 and hasNoSymbols(a) and len(key) > 1):
                if localMaxs(key, n_grams):
                    RE.append(key)
    return RE
	
