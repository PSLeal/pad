def p(A, doc):
    count = float (0)
    a = toString(A)
    for d in doc:
        count = count + len(re.findall(a, d))
    return count / len(corpus)   
	
def cov(A, B, prob, dList):
    a = float(1)/len(dList)
    mean = float(0)
    p_A_list = p(A, dList)
    p_B_list = p(B, dList)
    for doc in range(len(dList)):
        mean = mean + ( (prob.get(A)[doc] - p_A_list) * (prob.get(B)[doc] - p_B_list) )
    return a * mean

	
def corr(A, B, doc, dList):
    a = cov(A, B, doc, dList)
    b = float(math.sqrt(cov(A, A, doc, dList))) * math.sqrt(cov(B,B, doc, dList))
    if b == 0:
        return 0.0
    return float(a) / b
    
# calculates intra-document proximity
# assumes docs is an array of string arrays (documents)
def intraDocProx(A, B, docs):
    distsum = 0;
    
    parsed_docs = []
    relevant_docs = []

    for d in docs:
        parsed_docs.append(parseCorpus(d))
        
    relevant_docs = docs_contain(A, B, parsed_docs)

    if len(relevant_docs) == 0:
        return -1;

    for doc in relevant_docs:
        distsum += global_dist_farthest(A, B, doc)
   
    return 1-(1/len(relevant_docs)) * distsum;
	
# auxiliary function
# returns the global distance between A and B divided by the farthest distance that exist from A to B for document doc
# assumes doc must be an array of words
# assumes A and B must be tuples
# assumes A and B are both present in doc
def global_dist_farthest(A, B, doc):
    
    if A == B:
        return 0;
    
    # var positions is an array of tuples -> (char: 'A' or 'B', int: index in doc array)
    dist = 0;
    acounter = 0;
    bcounter = 0;
    nearest = len(doc);
    farthest = 1;
    auxmin = 0;
    positions = [];
    
    # search for A and B terms and register their index value in the positions array
    for counter in range(len(doc)-min(len(A), len(B))):
        if len(A) <= len(doc)-counter and tuple(doc[counter:counter+len(A)]) == A:
            positions.append(('A',counter));
            counter += len(A)
            acounter += 1;
        if len(B) <= len(doc)-counter and tuple(doc[counter:counter+len(B)]) == B:
            positions.append(('B',counter));
            counter += len(B);
            bcounter += 1;
    
    # sum minimum distances from B's to A's and the opposite likewise
    for occc in range(len(positions)):
        
        # for A terms, find the minimum distance to a B term and add it to the sum
        if positions[occc][0] == 'A':
            for left in range(occc-1, -1, -1):
                if positions[left][0] == 'B':
                    nearest = positions[occc][1] - positions[left][1] - (len(B) - 1);
            for right in range(occc+1, len(positions)):
                if positions[right][0] == 'B':
                    auxmin = positions[right][1] - positions[occc][1] - (len(A) - 1);
                    if auxmin < nearest:
                        nearest = auxmin;
        
        # for B terms, find the minimum distance to a A term and add it to the sum
        elif positions[occc][0] == 'B':
            for left in range(occc-1, -1, -1):
                if positions[left][0] == 'A':
                    nearest = positions[occc][1] - positions[left][1] - (len(A) - 1);
            for right in range(occc+1, len(positions)):
                if positions[right][0] == 'A':
                    auxmin = positions[right][1] - positions[occc][1] - (len(B) - 1);
                    if auxmin < nearest:
                        nearest = auxmin;
        
        else:
            continue;
            
        dist += nearest;
        nearest = len(doc);
    
    # Error messages
    if(acounter == 0):
        print("ERROR: No ocurrence of A in:")
        print(doc)
        
    if(bcounter == 0):
        if(acounter != 0):
            print("ERROR: No ocurrence of B in:")
            print(doc)
    
    # calculate farthest distance between an A and a B
    c1 = acounter * (len(doc) - bcounter);
    c2 = (((acounter - 1)**2) + acounter - 1)/float(2);
    c3 = bcounter * (len(doc) - acounter);
    c4 = (((bcounter - 1)**2) + bcounter - 1)/float(2);
    
    farthest = c1 - c2 + c3 - c4;
    
    return float(dist)/farthest;
	
# auxiliary function
# returns the documents that contain A and B terms (array)
def docs_contain(A, B, docs):
    doclist = [];
    
    for doc in docs:
        founda = False
        foundb = False
        # search for A and B terms in the document
        for counter in range(len(doc)-min(len(A), len(B))):
            if not founda and len(A) <= len(doc)-counter and tuple(doc[counter:counter+len(A)]) == A:
                founda = True
                counter += len(A)
            if not foundb and len(B) <= len(doc)-counter and tuple(doc[counter:counter+len(B)]) == B:
                foundb = True
                counter += len(B)
            if founda and foundb:
                doclist.append(doc)
                break

    return doclist;