import extractor as ex
import explicit as expl
import implicit as impl

txt = open("en1.8m.txt")
temp = txt.read()
sample_text = parseCorpus(temp)

corpus = sample_text

n_grams = ex.countFreq(corpus)

ex.calcGlue(n_grams)

REs = ex.extractConcepts(n_grams)

documents = re.split("<doc id=", temp)

t = []
for w in REs:
    leftmostW = w[0]
    rightmostW = w[len(w) -1]
    thres = 0.75 * len(documents)
    if checkWordApearance(leftmostW, documents) < thres and checkWordApearance(rightmostW, documents) < thres and len(leftmostW) > 2 and len(rightmostW) > 2:
        t.append(w)
REs = t

keys = []

for n in n_grams1[0].keys():
    if ex.hasNoSymbols(n) and n_grams1[0].get(n)[FREQ]> 1 and len(n[0]) > 5:
        keys.append(n)

#Listar
all_res = []
all_res.extend(REs)
all_res.extend(keys)

prob = dict()

for w in all_res:
    ws = toString(w)
    temp = []
    for doc in documents:
        #count = float ( sum(doc[i:i+len(w)]==w for i in range(len(doc))))
        count = len(re.findall(ws, doc))
        if count == 0.0:
            temp.append(0.0)
            continue
        size = float ( len(doc) )
        temp.append(count / size)
    if w != None:
        prob[w] = temp


parsed_docs = []

for d in documents:
    parsed_docs.append(parseCorpus(d))

#Implicitas:

text = 0
byText = []
SinglesText = []
MultiText = []
for i in ds[:1]:
    text = text + 1
    current = documents[1]
    curr_parsed = ex.parseCorpus(current)

    Multi = sorted(expl.tf_idf(REs, documents, current, len(curr_parsed)).items(), key=lambda t: t[1], reverse = True)[:5]
    MultiText.append(Multi)

    Singles = sorted(expl.tf_idf(keys, documents, current, len(curr_parsed)).items(), key=lambda t: t[1], reverse = True)[:5]
    SinglesText.append(Singles)
    expressions = []
    expressions.extend(Multi)
    expressions.extend(Singles)

    print len(expressions)
    if len(expressions) == 0:
        continue

    corrV = dict()


    #Para cada uma das keywords/keyconcepts, aplicar a correlação com todos os outros conceitos:
    for c in all_res:
        mean = 0.0
        if expl.toString(c) not in current:
            sys.stdout.flush()
            for RE in expressions:
                if len(impl.docs_contain(c, RE[0], parsed_docs)) == 0:
                    corrV[c] = 0.0
                    continue
                val = impl.corr(c, RE[0], prob, documents) * math.sqrt(impl.intraDocProx(c, RE[0], documents))
                mean = mean + val
            t = mean / len(expressions)
            corrV[c] = val
    byText.append(sorted(corrV.items(), key =lambda t : t[1], reverse = True)[:10])
    print "one text down"
